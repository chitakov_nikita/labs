package com.example.demo1;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * The type Login.
 */
public class Login extends HttpServlet {

    public void init() {
    }

    /**
     * Do POST HTTP Request to check data of User.
     *
     * @param request the request
     * @param response the response
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String userName = request.getParameter("userName");
        String password = request.getParameter("password");
        UserDAO userDAO = new UserDAO();
        UserAccount userAccount = userDAO.findUser(userName, password);
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        if (userAccount == null) {
            String errorMessage = "Invalid userName or Password";
            out.println("<html><body>");
            out.println("<h1>" + errorMessage + "</h1>");
            out.println("</body></html>");
            return;
        }
        out.println("<html><body>");
        out.println("<h1>" + "Ok" + "</h1>");
        out.println("</body></html>");
    }
    /**
     * Do GET HTTP Request to check data of User.
     *
     * @param request the request
     * @param response the response
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
    }

    public void destroy() {
    }
}