package com.example.demo1;

import java.sql.*;

/**
 * The type Connection manager.
 */
public class ConnectionManager {
    /**
     * The Jdbc driver.
     */
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    /**
     * The Jdbc db url.
     */
    static final String JDBC_DB_URL = "jdbc:mysql://localhost:3306/my_db";
    /**
     * The Jdbc user.
     */
    static final String JDBC_USER = "root";
    /**
     * The Jdbc pass.
     */
    static final String JDBC_PASS = "";
    private static Connection connection = null;

    /**
     * Gets connection.
     *
     * @return the connection
     */
    static public Connection getConnection() {
        if (connection == null) {
            try {
                Class.forName(JDBC_DRIVER);
                connection = DriverManager.getConnection(JDBC_DB_URL,
                        JDBC_USER, JDBC_PASS);
            } catch (SQLException | ClassNotFoundException exception) {
                exception.printStackTrace();
            }
        }
        return connection;
    }
}